# Welcome to the Atlassian Plugin SDK Source!

## Quick Links

* [How to Release][2]
* [Issues][1]
* [EcoBAC](https://ecosystem-bamboo.internal.atlassian.com/browse/AMPS)

## Using the Plugin SDK

Please consult our [Getting Started](https://developer.atlassian.com/display/DOCS/Getting+Started) documentation to learn how to build Atlassian Plugins. We also have a [quick-start tutorial](https://developer.atlassian.com/display/DOCS/Set+up+the+Atlassian+Plugin+SDK+and+Build+a+Project) for those who want to get stuck straight in to it.

## Reporting a Problem

If you're having problems with the Plugin SDK, please go to [Atlassian Answers](https://answers.atlassian.com), our Q&A community site, and ask for help. To report a bug, use the [AMPS JIRA Project][1]. If you're not sure if you've hit a bug, or just a usage problem, try Answers first!

## Contributing

This project is licensed under the [Apache 2.0 Software License](https://bitbucket.org/atlassian/amps/src/master/LICENSE). External contributors are welcome, but you must agree to the terms of the [Atlassian Contributor License Agreement](https://developer.atlassian.com/display/ABOUT/Atlassian+Contributor+License+Agreement) and then sign and submit a copy of the agreement to Atlassian.

### Rules of Engagement

If you want to submit bug fixes or improvements to the Plugin SDK, please assist us by adhering to these simple guidelines:

1. Do not commit directly to the primary branch. Please either fork this repo or use a 'feature branch' and then create a Pull Request for your changes to be accepted. The Developer Relations team will review and merge your work.
2. Create a JIRA Issue for any changes and prefix your commit messages with the relevant issue key.
3. You are responsible for testing and verifying your own changes. Yes, the Plugin SDK is a complex beast that must run on a variety of platforms against a variety of Atlassian products and must remain very backwards compatible and this means making changes can be difficult - please be diligent in this and help make life better for everyone who has to work on this project! :)

Atlassian developers, please see [The Platform Rules of Engagement (go/proe)](http://go.atlassian.com/proe).


[1]: https://ecosystem.atlassian.net/browse/AMPS
[2]: https://extranet.atlassian.com/pages/viewpage.action?pageId=2130217184
